﻿using System;
using System.IO;

namespace My_Vicelica
{
    class Program
    {
        static void Main(string[] args)
        {

            string path = @"E:\C# Learning\Vicelica\word_rus.txt";

            string[] wordsList = File.ReadAllLines(path);

            Random rand = new Random();
            while (true)
            {
                string word = wordsList[rand.Next(0, wordsList.Length)];
                char[] viewWord = new char[word.Length];

                for (int i = 0; i < word.Length; i++)
                {
                    viewWord[i] = '_';
                }

                int opennedLetters = 0;
                int errorCount = 7;
                Console.WriteLine($"Здравствуйте! Давайте сыграем в Виселицу!\nЗагадано слово из {word.Length} букв");
                while (errorCount > 0 && opennedLetters != word.Length)
                {
                    Console.Write("Введите букву:");
                    string userInput = Console.ReadLine();

                    if (userInput.Length != 1)
                    {
                        Console.WriteLine("Неверно введены данные.\nНеобходимо вводить только по одной букве.\nПопробуйте ещё раз");
                        continue;
                    }

                    if (!Char.IsLetter(userInput[0]))
                    {
                        Console.WriteLine("Неверно введены данные.\nНеобходимо вводить только буквы.\nПопробуйте ещё раз");
                        continue;
                    }

                    bool isLetterExist = false;

                    for (int i = 0; i < word.Length; i++)
                    {
                        Console.Clear();
                        Console.WriteLine(viewWord);
                        if (viewWord[i] == '_' && word[i] == userInput[0])
                        {
                            viewWord[i] = userInput[0];
                            opennedLetters++;
                            isLetterExist = true;
                        }
                    }

                    if (isLetterExist)
                    {
                        Console.WriteLine("Вы угадали букву!");
                    }
                    else
                    {
                        errorCount--;
                        Console.WriteLine($"К сожалению, такой буквы нет. У Вас осталось {errorCount} попыток."); ;
                    }

                }

                if (errorCount == 0)
                {
                    Console.WriteLine($"Вы проиграли! Правильно слово {word}.\nХотите попробовать ещё раз? (да/нет) ");
                }
                else
                {
                    Console.WriteLine("Поздравляю! Вы победили!\nСыграем ещё раз? (да/нет) ");
                }

                string agree = Console.ReadLine();

                if(agree == "да")
                {
                    Console.Clear();
                    continue;
                }
                else if(agree == "нет")
                {
                    break;
                }
                
            }
        }
    }
}